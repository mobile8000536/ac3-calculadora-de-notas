package com.impactacalc.frontend

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.impactacalc.R
import com.impactacalc.backend.calculaMedias
import com.impactacalc.backend.calculaNota

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DataNotas() {
    // Variáveis de dados
    var ac1 by remember{ mutableStateOf("") }
    var ac2 by remember{ mutableStateOf("") }
    var ac3 by remember{ mutableStateOf("") }
    var ac4 by remember{ mutableStateOf("") }
    var ac5 by remember{ mutableStateOf("") }
    var pai1 by remember{ mutableStateOf("") }
    var pai2 by remember{ mutableStateOf("") }
    var pai3 by remember{ mutableStateOf("") }
    var prova by remember{ mutableStateOf("") }
    var havePAI by remember { mutableStateOf(false)}
    var dataLayout by remember { mutableStateOf(true) }
    var mediaAc2 by remember { mutableStateOf("") }
    var mediaPai2 by remember { mutableStateOf("") }
    var isParcial2 by remember {mutableStateOf(true)}
    var pontoExtra by remember{ mutableStateOf("") }
    var mediaFinal by remember { mutableStateOf("") }
    var notaSub by remember{ mutableStateOf("") }

    //condicional para troca de tela
    if (dataLayout) {
        Column(
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(horizontal = 10.dp)
                .fillMaxSize()
        ) {

            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                // Botão para limpar os campos de notas
                Button(onClick = {
                    ac1 = ""
                    ac2 = ""
                    ac3 = ""
                    ac4 = ""
                    ac5 = ""
                    pai1 = ""
                    pai2 = ""
                    pai3 = ""
                    prova = ""
                    notaSub = ""
                    pontoExtra = ""
                    havePAI = false
                },
                    content = { Text(text = "Redefinir") })

                Image(
                    painter = painterResource(id = R.drawable.logo_impacta),
                    contentDescription = "Logo Impacta",
                    modifier = Modifier
                        .width(150.dp)
                        .height(100.dp)
                )
                Button(onClick = {

                    //Tratamento de Input acs
                    val acs = mutableListOf(ac1, ac2, ac3, ac4, ac5)
                    for (i in 4 downTo 0){if(acs[i]==""){acs.removeAt(i)}else{break}}
                    for(i in acs.indices){if(acs[i]==""){acs[i]="0"}}


                    val (mediaAc, mediaPai, isParcial) = if (havePAI) {

                        //Tratamento de Input Notas PAI
                        val pai = mutableListOf(pai1, pai2, pai3)
                        for (i in 2 downTo 0){if(pai[i]==""){pai.removeAt(i)}else{break}}
                        for(i in pai.indices){if(pai[i]==""){pai[i]="0"}}

                        //função que retorna as medias e se são parciais + tratamento de inputs vazios
                        calculaMedias(
                            if(acs.size==0){ mutableListOf("0")}else{acs},
                            if(pai.size==0){ mutableListOf("0")}else{pai})

                    } else {
                        calculaMedias(if(acs.size==0){ mutableListOf("0")}else{acs},null)
                    }

                    mediaAc2 = mediaAc.toString()
                    mediaPai2 = mediaPai.toString()
                    isParcial2 = (isParcial || prova =="")
                    if(prova==""){prova="0"}
                    mediaFinal = calculaNota(mediaAc,mediaPai,prova.toDouble()).toString()

                    dataLayout = false
                },
                    content = { Text(text = "Calcular") }
                )
            }

            Text(
                text = "Calculadora de Notas",
                fontSize = 26.sp
            )


            //INPUT BOX

            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = ac1,
                onValueChange = {ac1 = it},
                label = { Text("AC1") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )

            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = ac2,
                onValueChange = {ac2 = it},
                label = { Text("AC2") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )

            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = ac3,
                onValueChange = {ac3 = it },
                label = { Text("AC3") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )

            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = ac4,
                onValueChange = {ac4 = it },
                label = { Text("AC4") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )

            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = ac5,
                onValueChange = {ac5 = it },
                label = { Text("AC5") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )
            if (havePAI) {
                OutlinedTextField(
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    value = pai1,
                    onValueChange = {pai1 = it },
                    label = { Text("PAI 1") },
                    placeholder = {
                        Text(
                            text = "0.0", color = Color.Gray
                        )
                    },
                    singleLine = true,
                )

                OutlinedTextField(
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    value = pai2,
                    onValueChange = {pai2 = it },
                    label = { Text("PAI 2") },
                    placeholder = {
                        Text(
                            text = "0.0", color = Color.Gray
                        )
                    },
                    singleLine = true,
                )

                OutlinedTextField(
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    value = pai3,
                    onValueChange = {pai3 = it },
                    label = { Text("PAI 3") },
                    placeholder = {
                        Text(
                            text = "0.0", color = Color.Gray
                        )
                    },
                    singleLine = true,
                )
            }
            OutlinedTextField(keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                value = prova,
                onValueChange = {prova = it },
                label = { Text("Prova") },
                placeholder = {
                    Text(
                        text = "0.0", color = Color.Gray
                    )
                },
                singleLine = true
            )

            Spacer(modifier = Modifier.height(20.dp))

            Button(onClick = {
                havePAI = !havePAI
            },
                content = { Text(text = "PAI?") })
        }

    //tela 2 resultado
    }else{
        Column(
            Modifier
                .fillMaxSize()
                .padding(15.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(36.dp)
            ) {

                Button(onClick = {dataLayout=true}) {
                    Text(text = "Voltar")
                }
                Image(painter = painterResource(id = R.drawable.logo_impacta), contentDescription = "Logo impacta",
                    modifier = Modifier
                        .width(150.dp)
                        .height(100.dp) )
            }
            Spacer(modifier = Modifier.height(100.dp))

            //exibição das medias
            Text(text = "Média ACs: ${String.format("%.2f",mediaAc2.toDouble())}        ${
                if(havePAI){"Média PAI: ${String.format("%.2f",mediaPai2.toDouble())}"}else{""}}         ${
                    if(prova.toDouble()>=
                        if(notaSub==""){0.0}else{notaSub.toDouble()}
                    ){"Prova: $prova"}
                    else{"Sub: $notaSub"}
                }")

            //input ponto extra
            OutlinedTextField(value = pontoExtra, onValueChange = {pontoExtra=it}, label = { Text(
                text = "Ponto Extra",
            )},keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number), singleLine = true, placeholder = { Text(text = "0.0")})
            Spacer(modifier = Modifier.height(10.dp))

            //exibição da media final ou parcial + ponto extra (2 casas decimais)"
            Text(fontSize = 26.sp,text = "Media ${if(isParcial2){"parcial"}else{"final"}}: ${
                if(mediaFinal.toDouble() +
                    if(pontoExtra==""){0.0}
                    else{pontoExtra.toDouble()}>10.0)
                {"10.00"}
                else{String.format("%.2f",mediaFinal.toDouble() +
                        if(pontoExtra==""){0.0}
                        else{pontoExtra.toDouble()})}
            }")

            Spacer(modifier = Modifier.height(50.dp))

            //condicional para exibição de aprovado/reprovado + input SUB com botao
            if(!isParcial2){

                Text(fontSize = 26.sp,text = if(mediaFinal.toDouble()+if(pontoExtra==""){0.0}else{pontoExtra.toDouble()}>=5.0){"Aprovado"}else{"Reprovado"})

                // condicional para não exibir input de sub+botao se aprovado
                if(mediaFinal.toDouble()+if(pontoExtra==""){0.0}else{pontoExtra.toDouble()}<5.0){
                    OutlinedTextField(value = notaSub, onValueChange = {notaSub =it}, label = { Text(text = "Nota da Sub")}, placeholder = { Text(
                        text = "0.0"
                    )}, keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number), singleLine = true )
                    Button(onClick = {
                        mediaFinal = calculaNota(mediaAc2.toDouble(), if(!havePAI){null}else{mediaPai2.toDouble()}, notaSub.toDouble()).toString()
                    }) {
                        Text(text = "Calcular com SUB")
                    }
                }
            }

        }
    }
}
