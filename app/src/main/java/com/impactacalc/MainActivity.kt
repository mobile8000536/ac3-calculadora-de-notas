package com.impactacalc

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.impactacalc.frontend.DataNotas

import com.impactacalc.ui.theme.ImpactaCalcTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ImpactaCalcTheme {
                DataNotas()
            }
        }
    }
}
