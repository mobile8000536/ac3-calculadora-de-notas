package com.impactacalc.backend

fun calculaMedias(acs: MutableList<String>, pai:MutableList<String>?):Triple<Double,Double?,Boolean>{
    var isParcialAc = true
    var isParcialP = true

    // excluindo menor nota se inserido todas notas
    var acsDouble = acs.sortedByDescending { it.toDouble() }.toMutableList()
    if(acs.size==5){
        isParcialAc = false
        acsDouble = acsDouble.dropLast(1).toMutableList()}
    val acsSum = (acsDouble.sumOf{it.toDouble()})/acsDouble.size


    if (pai != null){
        var paiDouble = pai. sortedByDescending { it.toDouble() }.toMutableList()
        if(pai.size==3){
            isParcialP = false
            paiDouble = paiDouble.dropLast(1).toMutableList()}
        val paiSum = paiDouble.sumOf { it.toDouble() }/paiDouble.size

        return Triple(acsSum,paiSum,(isParcialAc || isParcialP))
    }
    return Triple(acsSum,null,isParcialAc)
}

fun calculaNota(mediaAc:Double, mediaPai:Double?, prova:Double) : Double {

    //somatório + peso da nota
    return if (mediaPai != null) {
        mediaAc * 0.3 + mediaPai * 0.3 + prova * 0.4
    } else {
        mediaAc * 0.5 + prova * 0.5
    }
}